#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://github.com/Palanthis
# License	:	Distributed under the terms of GNU GPL v3
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################

# Copy over some of my favorite fonts, themes and icons
sudo [ -d /usr/share/fonts/OTF ] || sudo mkdir /usr/share/fonts/OTF
sudo [ -d /usr/share/fonts/TTF ] || sudo mkdir /usr/share/fonts/TTF
sudo [ -d /usr/share/icons/Arch ] || sudo mkdir /usr/share/icons/Arch
sudo tar xzf tarballs/fonts-otf.tar.gz -C /usr/share/fonts/OTF/ --overwrite
sudo tar xzf tarballs/fonts-ttf.tar.gz -C /usr/share/fonts/TTF/ --overwrite
sudo tar xzf tarballs/buuf-icons.tar.gz -C /usr/share/icons/ --overwrite
sudo tar xzf tarballs/archlogo.tar.gz -C /usr/share/icons/Arch/ --overwrite
sudo tar xzf tarballs/oblogout.tar.gz -C /usr/share/themes/ --overwrite

# Write out new configuration files
tar xzf tarballs/config.tar.gz -C ~/ --overwrite
tar xzf tarballs/local.tar.gz -C ~/ --overwrite

# Copy over Menulibre items
tar xzf tarballs/applications.tar.gz -C ~/.local/share/ --overwrite
tar xzf tarballs/xfce-applications-menu.tar.gz -C ~/.config/menus/ --overwrite

# Install wallpapers
[ -d ~/Wallpapers ] || mkdir ~/Wallpapers
[ -d ~/Wallpapers/Abstract ] || mkdir ~/Wallpapers/Abstract
[ -d ~/Wallpapers/StarWarsWallpapers ] || mkdir ~/Wallpapers/StarWarsWallpapers
[ -d ~/Wallpapers/Sukhoi ] || mkdir ~/Wallpapers/Sukhoi
tar xzf tarballs/wallpapers1.tar.gz -C ~/Wallpapers/Abstract
#tar xzf tarballs/wallpapers2.tar.gz -C ~/Wallpapers/Abstract
tar xzf tarballs/starwarswallpapers.tar.gz -C ~/Wallpapers/StarWarsWallpapers/
tar xzf tarballs/sukhoi.tar.gz -C ~/Wallpapers/Sukhoi
